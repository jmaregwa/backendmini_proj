Requirements to run the code:
. Netbeans 8.1
. Apache Tomcat Server
. An account.dat file attached should be saved on the drive E.

The system has 3 endpoints, a balance end point, a deposit endpoint and a withdraw endpoint.
The account.dat file contains the account name, account number,balance account,maximum deposit
amount per day,amount already deposited,maximum deposit amount per transaction, number of times
deposit has already been done,maximun number of times a deposit can be done,maximum withdraw amount
per day,amount already withdrawn,maximum withdrawal amount per transaction,number of times a withdraw
has taken price,the maximum number of times one can withdraw from the account respectively.
The data is separated with commas.