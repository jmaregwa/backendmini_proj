package com.websystique.springmvc.controller;

import com.websystique.springmvc.model.Account;
import com.websystique.springmvc.model.Response;
import com.websystique.springmvc.model.Transaction;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.websystique.springmvc.service.AccountService;

@RestController
public class HelloWorldRestController {

    @Autowired
    AccountService accountService;

    //------getting balance
    @RequestMapping(value = "/balance/{account_no}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> getBalance(@PathVariable("account_no") String account_no) {
        System.out.println("Fetching Account with " + account_no);
        Account account = accountService.findByAccountNo(account_no);
        if (account == null) {
            System.out.println("Account number " + account_no + " not found");
            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Account>(account, HttpStatus.OK);
    }

    @RequestMapping(value = "/deposit/{account_no}/{amount}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> deposit(@PathVariable("account_no") String account_no,
            @PathVariable("amount") Double amount) {
        System.out.println("Depositing " + amount
                + " Account " + account_no);
        Account account = accountService.findByAccountNo(account_no);
        Response response = new Response();
        HttpHeaders headers = new HttpHeaders();
        if (account == null) {
            System.out.println("Account number " + account_no + " not found");
            response.setMessage("Account number " + account_no + " not found");
            response.setStatusCode(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<Response>(response, HttpStatus.NOT_FOUND);
        } else if (account.getDepositedaamount() <= account.getMax_deposit_day()) {
            if (amount <= account.getMax_deposit_trx()) {
                // System.out.print("Deposit freq"+account.getDepositfreq()+" maxfreq"+account.getMax_deposit_freq());
                if (account.getDepositfreq() < account.getMax_deposit_freq()) {
                    accountService.moveFunds(account, amount, "Deposit");
                    response.setMessage("Amount: " + amount + " deposited to acount " + account.getAccount_no());
                    response.setStatusCode(HttpStatus.ACCEPTED.value());
                    return new ResponseEntity<Response>(response, HttpStatus.ACCEPTED);
                } else {
                    response.setMessage("Today's Deposit times exceeded");
                    response.setStatusCode(HttpStatus.NOT_ACCEPTABLE.value());
                    return new ResponseEntity<Response>(response, HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                response.setMessage("Today's Deposit Limit exceeded");
                response.setStatusCode(HttpStatus.NOT_ACCEPTABLE.value());
                return new ResponseEntity<Response>(response, HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            response.setMessage("Maximum Amount Deposit Per day exceeded");
            response.setStatusCode(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<Response>(response, HttpStatus.NOT_FOUND);
        }

        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
    }

    //----------Withdrawal Request------------------
    @RequestMapping(value = "/withdraw/{account_no}/{amount}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> withdraw(@PathVariable("account_no") String account_no,
            @PathVariable("amount") Double amount) {
        System.out.println("Withdrawing " + amount
                + " Account " + account_no);
        Account account = accountService.findByAccountNo(account_no);
        Response response = new Response();
        HttpHeaders headers = new HttpHeaders();
        if (account == null) {
            System.out.println("Account number " + account_no + " not found");
            response.setMessage("Account number " + account_no + " not found");
            response.setStatusCode(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<Response>(response, HttpStatus.NOT_FOUND);

        } else if (account.getWithdrawnamount() <= account.getMax_withd_day()) {
            System.out.println("trx" + account.getMax_withd_trx() + "an amount" + amount);
            if (amount <= account.getMax_withd_trx()) {
                // System.out.print("Withdraw freq"+account.getDepositfreq()+" maxfreq"+account.getMax_deposit_freq());
                if (account.getWithdrawfreq() < account.getMax_withd_freq()) {
                    accountService.moveFunds(account, amount, "Withdrwal");
                    response.setMessage("Amount: " + amount + " withdrawn from " + account.getAccount_no());
                    response.setStatusCode(HttpStatus.ACCEPTED.value());
                    return new ResponseEntity<Response>(response, HttpStatus.ACCEPTED);
                } else {
                    response.setMessage("Withdrwal times exceeded");
                    response.setStatusCode(HttpStatus.NOT_ACCEPTABLE.value());
                    return new ResponseEntity<Response>(response, HttpStatus.NOT_ACCEPTABLE);
                }
            } else {
                response.setMessage("Maximum Withdrawal Amount per transaction exceeded");
                response.setStatusCode(HttpStatus.NOT_ACCEPTABLE.value());
                return new ResponseEntity<Response>(response, HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            response.setMessage("Maximum Withdrwal Amount Per day reached");
            response.setStatusCode(HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<Response>(response, HttpStatus.NOT_FOUND);
        }

        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
    }

}
