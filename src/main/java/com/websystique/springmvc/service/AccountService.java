/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.service;

import com.websystique.springmvc.model.Account;
import com.websystique.springmvc.model.Transaction;

/**
 *
 * @author gkosgei
 */
public interface AccountService {

    Account findByAccountNo(String account_no);

    Boolean moveFunds(Account account, Double amount,  String trxType);

}
