/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.service;

import com.websystique.springmvc.model.Account;
import com.websystique.springmvc.model.ReadUpdateFile;
import com.websystique.springmvc.model.Transaction;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jane
 */
@Service("accountService")
@Transactional
public class AccountServiceImpl implements AccountService {

    private static List<Account> accounts;

    static {
        accounts = populateDummyAccounts();
    }

    public Account findByAccountNo(String account_no) {
        for (Account account : accounts) {
            if (account.getAccount_no().equalsIgnoreCase(account_no)) {
                return account;
            }
        }
        return null;
    }
    
    private static List<Account> populateDummyAccounts() {
        List<Account> accounts = new ArrayList<Account>();
        ReadUpdateFile file = new ReadUpdateFile();
        String str = file.read();
        
        System.out.println("file" + str);
        if (!str.isEmpty()) {
            String[] account_details = str.split(",");
            accounts.add(new Account(
                    account_details[0],
                    account_details[1],
                    Double.parseDouble(account_details[2]),
                    Double.parseDouble(account_details[3]),
                    Double.parseDouble(account_details[4]),
                    Double.parseDouble(account_details[5]),
                    Double.parseDouble(account_details[6]),
                    Double.parseDouble(account_details[7]),
                    Double.parseDouble(account_details[8]),
                    Double.parseDouble(account_details[9]),
                    Double.parseDouble(account_details[10]),
                    Double.parseDouble(account_details[11]),
                    Double.parseDouble(account_details[12])
            ));
        }
        return accounts;
    }

    public synchronized  Boolean moveFunds(Account account, Double amount,  String trxType) {
        Double bal = 0.0;
        Double deptrans=0.0;
        Double withtrans=0.0;
        Double depositamount=0.0;
        Double withdrwalamount=0.0;
        if (trxType.equalsIgnoreCase("deposit")) {
            bal = account.getBalance();
            bal+=amount;
            deptrans=account.getDepositfreq();
            deptrans+=1;
            depositamount=account.getDepositedaamount();
            depositamount+=amount;
            
            account.setDepositfreq(deptrans);
            account.setDepositedaamount(depositamount);
            
            
        } 
        else {
            bal = account.getBalance();
            bal-=amount;
            withtrans=account.getWithdrawfreq();
            withtrans+=1;
            withdrwalamount=account.getWithdrawnamount();
            withdrwalamount+=amount;
            account.setWithdrawnamount(withdrwalamount);
            account.setWithdrawfreq(withtrans);
        }
        
        account.setBalance(bal);
        System.out.println("New balance is " + account.getBalance());
        ReadUpdateFile file = new ReadUpdateFile();
        System.out.println("Writing to file");
        file.write(account.toString());
        return true;        
    }
    
    
   
}
