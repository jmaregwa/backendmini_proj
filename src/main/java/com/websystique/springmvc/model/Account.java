/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.model;

/**
 *
 * @author jane
 */
public class Account {
    private String name;
    private String account_no;
    private double balance;
    private double max_deposit_day;
    private double max_deposit_trx;
    private double max_deposit_freq; 
    private double max_withd_day;
    private double max_withd_trx;
    private double max_withd_freq;
    private double depositfreq;
    private double withdrawfreq;
    private double withdrawnamount;
    private double depositedaamount;
public Account() {}
    public Account(String name, String account_no, double balance, 
            double max_deposit_day,double deposited_amount, double max_deposit_trx,double deposit_freq, 
            double max_deposit_freq, double max_withd_day, double withdrawn_amount,
            double max_withd_trx, double withdraw_freq,double max_withd_freq) {
        
        this.name = name;
        this.account_no = account_no;
        this.balance = balance;
        this.max_deposit_day = max_deposit_day;
        this.max_deposit_trx = max_deposit_trx;
        this.depositfreq=deposit_freq;
        this.max_deposit_freq = max_deposit_freq;
        this.max_withd_day = max_withd_day;
        this.max_withd_trx = max_withd_trx;
        this.withdrawfreq=withdraw_freq;
        this.max_withd_freq = max_withd_freq;
        this.depositedaamount=deposited_amount;
        this.withdrawnamount=withdrawn_amount;
    }

    
    
    public String getName() {
        return name;
    }

    public String getAccount_no() {
        return account_no;
    }

    public double getBalance() {
        return balance;
    }

    public double getMax_deposit_day() {
        return max_deposit_day;
    }

    public void setDepositfreq(double depositfreq) {
        this.depositfreq = depositfreq;
    }

    public void setWithdrawnamount(double withdrawnamount) {
        this.withdrawnamount = withdrawnamount;
    }

    public void setDepositedaamount(double depositedaamount) {
        this.depositedaamount = depositedaamount;
    }

    public double getWithdrawnamount() {
        return withdrawnamount;
    }

    public double getDepositedaamount() {
        return depositedaamount;
    }

    public void setWithdrawfreq(double withdrawfreq) {
        this.withdrawfreq = withdrawfreq;
    }

    public double getDepositfreq() {
        return depositfreq;
    }

    public double getWithdrawfreq() {
        return withdrawfreq;
    }

    public double getMax_deposit_trx() {
        return max_deposit_trx;
    }

    public double getMax_deposit_freq() {
        return max_deposit_freq;
    }

    public double getMax_withd_day() {
        return max_withd_day;
    }

    public double getMax_withd_trx() {
        return max_withd_trx;
    }

    public double getMax_withd_freq() {
        return max_withd_freq;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setMax_deposit_day(double max_deposit_day) {
        this.max_deposit_day = max_deposit_day;
    }

    public void setMax_deposit_trx(double max_deposit_trx) {
        this.max_deposit_trx = max_deposit_trx;
    }

    public void setMax_deposit_freq(double max_deposit_freq) {
        this.max_deposit_freq = max_deposit_freq;
    }

    public void setMax_withd_day(double max_withd_day) {
        this.max_withd_day = max_withd_day;
    }

    public void setMax_withd_trx(double max_withd_trx) {
        this.max_withd_trx = max_withd_trx;
    }

    public void setMax_withd_freq(double max_withd_freq) {
        this.max_withd_freq = max_withd_freq;
    }
    
    
    
    
    @Override
    public String toString() {
        return  name + "," + account_no + "," + balance + "," + max_deposit_day 
                + "," + depositedaamount+ "," + max_deposit_trx + "," + depositfreq + "," + max_deposit_freq + "," 
                + max_withd_day + "," + withdrawnamount+ "," + max_withd_trx + "," + withdrawfreq + "," + max_withd_freq;
    }
    
    

}
