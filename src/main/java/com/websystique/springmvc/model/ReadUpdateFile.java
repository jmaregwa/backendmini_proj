/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.websystique.springmvc.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author jane
 */
public class ReadUpdateFile {

   // public String defaultaccountFile = "E:/account.dat";//default
    public String accountFile = "E:/account.dat";
    public int loaded = 0;

    public void write(String str) {
        /*if (!createwritetodaysfile()) {
            accountFile = defaultaccountFile;
        }*/
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(accountFile, false));
            bw.write(str);
        } catch (Exception e) {
            return;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {

            }
        }

    }

    public String read() {
         /*if (!createwritetodaysfile()) {
            accountFile = defaultaccountFile;
        }*/
        String line;
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(accountFile));
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

   
}
