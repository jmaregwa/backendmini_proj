package com.websystique.springmvc;


import org.springframework.web.client.RestTemplate;


import com.websystique.springmvc.model.Account;
import com.websystique.springmvc.model.Response;

public class SpringRestTestClient1 {
        public static final String REST_SERVICE_URI = "http://localhost:8086/Spring4MVCCRUDRestService";
	public static String account_no="12345";
        public static String depositamount="1000";
        public static String withdrawmount="5000";
        
        private static void getBalance(){
            System.out.println("Testing getBalance API----------");
            RestTemplate restTemplate = new RestTemplate();
            Account account = restTemplate.getForObject(REST_SERVICE_URI+"/balance/"+account_no, Account.class);
            System.out.println("Account Number: "+account.getAccount_no()+" Balance : $ "+account.getBalance());
        }
    
     private static void deposit(){
            System.out.println("Deposit API----------");
            RestTemplate restTemplate = new RestTemplate();
            Response response = restTemplate.getForObject(REST_SERVICE_URI+"/deposit/"+account_no+"/"+depositamount, Response.class);
            System.out.println("Response: "+response.getMessage()+" Status: "+response.getStatusCode());
        }
     
      private static void withdraw(){
            System.out.println("Withdraw API----------");
            RestTemplate restTemplate = new RestTemplate();
            Response response = restTemplate.getForObject(REST_SERVICE_URI+"/withdraw/"+account_no+"/"+withdrawmount, Response.class);
            System.out.println("Response: "+response.getMessage()+" Status: "+response.getStatusCode());
        }
    

    public static void main(String args[]){
            getBalance();
            deposit();
            withdraw();
    }
}